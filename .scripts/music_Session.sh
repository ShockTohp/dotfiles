#! /bin/bash

SESSION="music"
SESSIONEXISTS=$(tmux list-sessions | grep $SESSION)

if [ "$SESSIONEXISTS" = "" ]
then
	tmux new-session -d -s $SESSION -x 256 -y 29
	tmux rename-window -t $SESSION Controls
	tmux send-keys -t $SESSION 'ncmpcpp' C-m

	tmux split-window -v -t $SESSION

	tmux send-keys -t $SESSION.bottom 'ncmpcpp --screen clock -S visualizer' C-m

	tmux send-keys -t $SESSION C-b Up


fi
